package com.example.gestitb

import android.app.AlertDialog
import android.app.AlertDialog.Builder
import android.app.Dialog
import android.os.Bundle
import android.view.View
import android.widget.*
import androidx.fragment.app.DialogFragment
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import java.util.*

class GestFragment: Fragment (R.layout.gest_fragment) {
    private lateinit var addButton : Button
    private lateinit var dateButton: Button
    private lateinit var nameField: EditText
    private lateinit var detailsField: Spinner
    private lateinit var justifiedBool: CheckBox

    private var missedAttendances = mutableListOf<MissedAttendanceClass>()

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        addButton = view.findViewById(R.id.addButton)
        dateButton = view.findViewById(R.id.dateButton)
        nameField = view.findViewById(R.id.editTextTextPersonName)
        detailsField = view.findViewById(R.id.spinner1)
        justifiedBool = view.findViewById(R.id.checkBox1)


        var cal = Calendar.getInstance().time
        dateButton.text = cal.toString()

        val module = detailsField.selectedItem.toString()
        var justifiedMiss = "";

        justifiedMiss = if (justifiedBool.isChecked){
            "without justification"
        }else{
            "with justification"
        }


        val dialog = Builder(this.context)

        dialog.setTitle("Missed attendance created")
            .setMessage("The student ${nameField.text} has missed $module on ${dateButton.text} $justifiedMiss")
            .setPositiveButton("OK") { view, _ ->
                view.dismiss()
            }
            .setCancelable(false)
            .create()

        addButton.setOnClickListener {
            //missedAttendances.add(MissedAttendanceClass(nameField.toString(), module, dateButton.text(), justifiedBool.isChecked))
            dialog.show()
        }
    }
}