package com.example.gestitb

import java.util.*

data class MissedAttendanceClass(val name: String, val details: String, val date: Date, val justified: Boolean)